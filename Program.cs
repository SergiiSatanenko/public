﻿using System;

namespace matrix2
{
    class Program
    {
        static void Main(string[] args)
        {                       
            var controlMartrix = new int[3, 3] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
            var x = new int();
            var y = new int();
            var answerString = "";
            var answer = new int();

            do
            {
                Console.WriteLine("1. Проверка работы на матрице по умолчанию");
                Console.WriteLine("2. Ввод параметров матрицы");
                Console.WriteLine("3. Выход");
                answerString = Console.ReadLine();
                Console.Clear();
                bool wasparsed = int.TryParse(answerString, out answer);
                if (wasparsed)
                {
                    if (answer == 1)
                    {
                        MatrixCalc defM = new MatrixCalc(controlMartrix);
                        Console.Write("Сумма главной диагонали: ");
                        Console.WriteLine(defM.CalcMatrix());

                        Console.WriteLine("Матрица:");
                        defM.PrintMatrix();
                    }
                    else if (answer == 2)
                    {
                        //enter matrix param
                        Console.WriteLine("Введите размерность матрицы");
                        Console.Write("Введите количество строк:");
                        do
                        {
                            answerString = Console.ReadLine();
                            bool wasparsed1 = int.TryParse(answerString, out x);
                            if (wasparsed1)
                            {
                                if (x < 2)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("--ERROR: Значение не может быть меньше 2");
                                    Console.ResetColor();
                                    Console.Write("Введите количество строк:");
                                }
                                else Console.WriteLine("Спасибо");
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("--ERROR: Введено не верное значение!!!");
                                Console.ResetColor();
                                Console.Write("Введите количество строк:");
                            }
                        } while (x < 2);
                        Console.Clear();
                        Console.Write("Введите количество столбиков:");

                        do
                        {
                            answerString = Console.ReadLine();
                            bool wasparsed1 = int.TryParse(answerString, out y);
                            if (wasparsed1)
                            {
                                if (y < 2)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("--ERROR: Значение не может быть меньше 2");
                                    Console.ResetColor();
                                    Console.Write("Введите количество столбиков:");
                                }
                                else Console.WriteLine("Спасибо");
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("--ERROR: Введено не верное значение!!!");
                                Console.ResetColor();
                                Console.Write("Введите количество столбиков:");
                            }
                        } while (y < 2);
                        Console.Clear();

                        var c = new MatrixCalc(x, y);
                        Console.Write("Сумма главной диагонали: ");
                        Console.WriteLine(c.CalcMatrix());

                        Console.WriteLine("Матрица:");
                        c.PrintMatrix();
                    }

                    else if (answer == 3)
                    {
                        Console.WriteLine("Exit...");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("--ERROR: Введено не верное значение!!!");
                        Console.ResetColor();
                    }

                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("--ERROR: Введено не верное значение!!!");
                    Console.ResetColor();
                }
                Console.WriteLine("Для продолжения нажмите Enter");
                Console.ReadLine(); Console.Clear();
            } while (answer != 3); return;               
        }        
    }
}
